import java.io.File
import java.io.PrintWriter

// Найти количество разных подстрок в строке

fun main() {
    val outputWriter = PrintWriter(File("output.dat"))

    File("input.dat").forEachLine {
        outputWriter.println("String: $it")
        println("String: $it")

        val suffixArray = makeSuffixArray(it)
        val lcp = makeLCP(it)

        val arrayResult = suffixArray
            .fold(
                initial = 0,
                operation = { acc, i -> acc + suffixArray.size - suffixArray[i] })
            .minus(
                lcp.drop(1).fold(
                    initial = 0,
                    operation = { acc, i -> acc + i })
            )

        outputWriter.println("Result with LCP: $arrayResult words")
        println("Result with LCP: $arrayResult words")

        val suffixAutomaton = SuffixAutomaton(it)
        val cache = mutableMapOf<SuffixAutomaton.State, Int>()
        val automatonResult = count(suffixAutomaton.states.first(), cache)

        outputWriter.println("Result with automaton: $automatonResult words")
        println("Result with automaton: $automatonResult words")

//        Uncomment for automaton rendering
//        suffixAutomaton.render("$it.png", it)
    }

    outputWriter.close()
}

/**
 * Counts number of substrings with suffix automaton and memoization.
 */
fun count(current: SuffixAutomaton.State, cache: MutableMap<SuffixAutomaton.State, Int>): Int {
    if (current in cache) {
        return cache[current]!!
    }

    val sum = current.transitions.values.fold(initial = 0,
        operation = { acc, state ->
            acc + count(state, cache) + 1
        })

    cache[current] = sum

    return sum
}