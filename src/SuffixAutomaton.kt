/**
 * Minimal deterministic finite automation,
 * which accepts all suffixes of string and only them.
 */
class SuffixAutomaton() {
    /**
     * State of suffix automation.
     */
    interface State {
        /**
         * Length of longest string, which is accepted by state.
         */
        val length: Int
        /**
         * Suffix link to state, which accepts given string without first character.
         */
        val link: State?
        /**
         * All valid transitions between states by given characters.
         */
        val transitions: Map<Char, State>
    }

    private class MutableState(override var length: Int = 0, override var link: MutableState? = null) : State {
        override val transitions = mutableMapOf<Char, MutableState>()

        fun copy(): MutableState {
            return MutableState(length, link).also {
                it.transitions.putAll(transitions)
            }
        }

        override fun equals(other: Any?): Boolean {
            if (this === other) return true
            if (javaClass != other?.javaClass) return false

            other as MutableState

            if (length != other.length) return false
            if (link != other.link) return false
            if (transitions != other.transitions) return false

            return true
        }

//        Хэш для мутабельной коллекции посчитать нельзя!
//        override fun hashCode(): Int {
//            var result = length
//            result = 31 * result + (link?.hashCode() ?: 0)
//            result = 31 * result + transitions.hashCode()
//            return result
//        }
    }

    private val _states = mutableListOf<MutableState>()
    private var last: MutableState

    /**
     * Enumerated states of automation, where the first state is the initial.
     */
    val states: List<State>
        get() = _states

    init {
        // Initial state
        _states.add(MutableState())
        last = _states[0]
    }

    /**
     * Builds automaton from given string.
     */
    constructor(string: String) : this() {
        for (c in string) {
            addChar(c)
        }
    }

    private fun newState(): MutableState {
        _states.add(MutableState(length = last.length + 1, link = _states[0]))

        return _states.last()
    }

    private fun copyState(q: MutableState): MutableState {
        _states.add(q.copy())

        return _states.last()
    }

    /**
     * Appends new state and rebuilds the automaton.
     */
    fun addChar(c: Char) {
        val cur = newState()

        var p: MutableState? = last
        while (p != null && c !in p.transitions) {
            p.transitions[c] = cur
            p = p.link
        }

        if (p != null) {
            val q = p.transitions[c]
            if (p.length + 1 == q!!.length) {
                cur.link = q
            } else {
                val new = copyState(q)
                new.length = p.length + 1
                q.link = new
                cur.link = new

                while (p != null && p.transitions[c] == q) {
                    p.transitions[c] = new
                    p = p.link
                }
            }
        }

        last = cur
    }
}