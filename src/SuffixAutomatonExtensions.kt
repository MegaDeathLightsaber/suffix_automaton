import java.awt.Color
import java.awt.image.BufferedImage
import java.io.File
import javax.imageio.ImageIO

const val TILE_SIZE = 64
/**
 * Finds state which accept given string.
 *
 * @return state, or `null` if string isn't accepted by automaton.
 */
fun SuffixAutomaton.findAcceptanceOf(string: String): SuffixAutomaton.State? {
    var currentState = states[0]

    for (c in string) {
        if (c in currentState.transitions) {
            currentState = currentState.transitions.getValue(c)
        } else {
            return null
        }
    }

    return currentState
}

/**
 * Displays automaton to image file.
 */
fun SuffixAutomaton.render(fileName: String, accepted: String = "") {
    val size = (states.size + 1) * TILE_SIZE
    val image = BufferedImage(size, size, BufferedImage.TYPE_INT_RGB)

    image.graphics.run {
        color = Color.WHITE
        fillRect(0, 0, size, size)

        // Draw table
        color = Color.black
        for (i in states.indices) {
            drawLine((i + 1) * TILE_SIZE, 0, (i + 1) * TILE_SIZE, size)
            drawLine(0, (i + 1) * TILE_SIZE, size, (i + 1) * TILE_SIZE)
        }
        drawLine(0, 0, TILE_SIZE, TILE_SIZE)

        drawString("q", (2 * TILE_SIZE) / 3, TILE_SIZE / 3)
        drawString("p", TILE_SIZE / 4, (3 * TILE_SIZE) / 4)

        for (i in states.indices) {
            // Draw states
            drawString("$i", TILE_SIZE / 2, (i + 1) * TILE_SIZE + TILE_SIZE / 2)
            drawString("$i", (i + 1) * TILE_SIZE + TILE_SIZE / 2, TILE_SIZE / 2)

            // Draw transitions
            for (e in states[i].transitions) {
                drawString(
                    "${e.key}",
                    (states.indexOf(e.value) + 1) * TILE_SIZE + TILE_SIZE / 2,
                    (i + 1) * TILE_SIZE + TILE_SIZE / 2
                )
            }
        }

        // Highlight terminal states
        color = Color(128, 128, 128, 128)
        // TODO: change to looping through iterator
        var terminal = findAcceptanceOf(accepted)
        while (terminal != null) {
            val index = states.indexOf(terminal)

            fillRect((index + 1) * TILE_SIZE, 0, TILE_SIZE, TILE_SIZE)
            fillRect(0, (index + 1) * TILE_SIZE, TILE_SIZE, TILE_SIZE)

            terminal = terminal.link
        }

        dispose()
    }

    val file = File(fileName)
    val (_, format) = fileName.split('.')
    ImageIO.write(image, format, file)
}