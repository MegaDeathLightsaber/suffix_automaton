/**
 * Builds list of suffix indices of string, given in lexicographic order of corresponding suffixes.
 */
fun makeSuffixArray(string: String): List<Int> {
    val array = string.indices.sortedWith(object : Comparator<Int> {
        override fun compare(o1: Int?, o2: Int?): Int {
            for (i in 0..(string.length - 1 - maxOf(o1!!, o2!!))) {
                if (string[(o1 + i) % string.length] > string[(o2 + i) % string.length]) {
                    return 1
                }

                if (string[(o1 + i) % string.length] < string[(o2 + i) % string.length]) {
                    return -1
                }
            }

            if (o1 < o2) return 1
            if (o1 > o2) return -1

            return 0
        }
    })

    return array
}

/**
 * Builds list of lengths of longest common prefixes
 * between `s[i-1]` and `s[i]` elements of suffix array of string.
 */
fun makeLCP(string: String): List<Int> {
    // Append neutral element to the end of given string
    val actualString = string + Character.MIN_VALUE
    val n = actualString.length

    val lcp = MutableList(n) { 0 }

    val suf = makeSuffixArray(actualString)

    // Inverse suffix array
    val rank = MutableList(n) { -1 }
    for ((index, value) in suf.withIndex()) {
        rank[value] = index
    }

    var k = 0
    for (i in 0 until n) {
        if (k > 0)
            --k

        if (rank[i] == n - 1) {
            lcp[n - 1] = -1
            k = 0
            continue
        }

        val j = suf[rank[i] + 1]
        while (maxOf(i + k, j + k) < n && actualString[i + k] == actualString[j + k])
            k++
        lcp[rank[i]] = k
    }

    return lcp.dropLast(1)
}